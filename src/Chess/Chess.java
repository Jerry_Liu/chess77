package Chess;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Chess {
	private static ArrayList<Piece> board = new ArrayList<Piece>(64);
	private static int wKing, bKing; //respective king locations for use with Check checking
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String raw;
		String thirdTok = "";
		int turnCounter = 0; //0 means White's turn, 1 means Black's turn
		int origin, destination;
		char c;
		boolean illegalMove = true;
		int drawStatus = 0;
		
		setUp();
		
		//main game loop
		while(true) { //remember to change to checkmate and close scanner
			drawBoard();
			
			//clean input loop
			do {
				if (turnCounter == 0) {
					System.out.print("White's move: ");
				}
				else {
					System.out.print("Black's move: ");
				}
				
				raw = sc.nextLine();
				System.out.println("");
			} while(!checkInput(raw));
			
			if (raw.equals("resign")) {
				break;
			}
			else if(raw.equals("draw") && drawStatus == 1) {
				break;
			}
			
			drawStatus = 0;
			
			String[] split = raw.split(" ");
			origin = coordtoInt(split[0]);
			destination = coordtoInt(split[1]);
			
			if (split.length > 2) {
				thirdTok = split[2];
			}
			
			if (thirdTok.equals("draw?")) {
				drawStatus = 1;
			}
			
			//loop for checking if move is legal and executing said move
			do {
				if(checkMove(turnCounter, origin, destination)) {
					illegalMove = false;
					int[] squares = board.get(origin).move(destination);
					for (int x = 0; x < squares.length; x++) {
						if (board.get(squares[x]) != null) {
							illegalMove = true;
							destination = origin;
							break;
						}
					}
					
					//checks to see if pieces at origin and destination are same color. If not, executes move
					if (!illegalMove) {
						//pawn case
						if (board.get(origin) instanceof Pawn) {
							if (pawnMovement(origin, destination)) {
								illegalMove = true;
								destination = origin;
							}
							else {
								executeMove(origin, destination);
								
								if (turnCounter == 0) {
									c = 'w';
								}
								else {
									c = 'b';
								}
								if (((destination / 8 == 7) && turnCounter == 0) || ((destination / 8 == 0) && turnCounter == 1)) {
									if (thirdTok.equals("R")) {
										board.set(destination, new Rook(c, destination));
									}
									else if(thirdTok.equals("N")) {
										board.set(destination,  new Knight(c, destination));
									}
									else if(thirdTok.equals("B")) {
										board.set(destination, new Bishop(c, destination));
									}
									else {
										board.set(destination, new Queen(c, destination));
									}
								}
							}
						}
						//castle case
						else if (board.get(origin) instanceof King && board.get(origin).move(destination).length > 0 && board.get(origin).move(destination)[0] != -1) {
							if (origin > destination) {
								if (board.get(origin - 4) == null || board.get(origin - 4).getMoved()) {
									illegalMove = true;
									destination = origin;
								}
								else {
									executeMove(origin, destination);
									executeMove(origin - 4, destination + 1);
								}
							}
							else {
								if (board.get(origin + 3) == null || board.get(origin + 3).getMoved()) {
									illegalMove = true;
									destination = origin;
								}
								else {
									executeMove(origin, destination);
									executeMove(origin + 3, destination - 1);
								}
							}
						}
						else if (board.get(destination) == null) {
							executeMove(origin, destination);
						}
						else {
							if (board.get(destination).getColor() == board.get(origin).getColor()) {
								illegalMove = true;
								destination = origin;
							}
							else {
								board.set(destination, null);
								executeMove(origin, destination);
							}
						}
					}	
				}	
				else {
					do {
						System.out.print("Illegal move, try again: ");
						raw = sc.nextLine();
						System.out.println("");
					} while(!checkInput(raw));
					
					split = raw.split(" ");
					origin = coordtoInt(split[0]);
					destination = coordtoInt(split[1]);
					
					illegalMove = true;
				}
			} while(illegalMove);
			
			int checker = checkCheck(turnCounter);
			
			if (checker == 1) {
				System.out.println("Check\n");
			}
			turnCounter = 1 - turnCounter;
		}
		
		if (drawStatus == 1) {
			System.out.println("Draw");
		}
		else if (turnCounter == 0) {
			System.out.println("Black wins");
		}
		else {
			System.out.println("White wins");
		}
		
		sc.close();
	}
	
	public static int checkCheck(int turnCounter) {
		int victimKing;
		int squares[];
		boolean check = false;
		
		if(turnCounter == 0) {
			victimKing = bKing;
		}
		else {
			victimKing = wKing;
		}
		
		for(int x = 0; x < 64;  x++) {
			if (board.get(x) != null) {
				if (checkMove(turnCounter, x, victimKing)) {
					squares = board.get(x).move(victimKing);
					
					for (int y = 0; y < squares.length; y++) {
						if (board.get(squares[y]) != null) {
							break;
						}
						if (y == squares.length - 1 ) {
							check = true;
						}
					}
				}
			}
		}
		
		if (check) {
			return 1;
		}
		else {
			return 0;
		}
	}
	
	public static void executeMove(int origin, int destination) {
		if(board.get(destination) == null) {
			Collections.swap(board,  origin,  destination);
		}
		else {		
			board.set(destination, null);
			Collections.swap(board, origin, destination);
		}
		
		if (board.get(destination) instanceof King) {
			if (board.get(destination).getColor() == 'w') {
				wKing = destination;
			}
			else {
				bKing = destination;
			}
		}
		
		board.get(destination).setMoved();
		board.get(destination).setLocation(destination);
	}
	
	//returns false if move is good, true otherwise
	public static boolean pawnMovement(int origin, int destination) {
		if (origin % 8 == destination % 8) {
			if (board.get(destination) == null) {
				return false;
			}
			else {
				return true;
			}
		}
		else {
			if (board.get(destination) == null) {
				return true;
			}
			else {
				if (board.get(destination).getColor() == board.get(origin).getColor()) {
					return true;
				}
				else {
					return false;
				}
			}
		}
	}
	
	//checks if piece at origin exists, if it's the right color, and if move function returns a success
	public static boolean checkMove(int turnCounter, int origin, int destination) {
		boolean rightColor = false;
		
		if (board.get(origin) != null) {
			if ((board.get(origin).getColor() == 'w' && turnCounter == 0) || board.get(origin).getColor() == 'b' && turnCounter == 1) {
				rightColor = true;
			}
		}

		
		return (board.get(origin) != null) &&
				rightColor &&
				(origin != destination) &&
				(board.get(origin).move(destination).length == 0 || board.get(origin).move(destination)[0] != -1);
	}
	
	//returns true if input is legal, false otherwise
	public static boolean checkInput(String in) {
		String[] tok = in.split(" ");
		
		if (tok.length > 3) {
			return false;
		}
		else if(tok[0].equals("resign")) {
			
		}
		else if(tok[0].equals("draw")) {
			
		}
		else if(!checkCoords(tok[0]) || !checkCoords(tok[1])) {
			return false;
		}
		return true;
	}
	
	//returns true if input is a legal set of coordinates, false otherwise
	public static boolean checkCoords(String in) {
		if (in.length() != 2) {
			System.out.println("Not acceptable coordinates\n");
			return false;
		}
		
		char file = Character.toLowerCase(in.charAt(0));
		char rank = in.charAt(1);
		
		if (file < 97 || file > 104) {
			System.out.println("Not acceptable coordinates\n");
			return false;
		}
		else if(rank < 49 || rank > 56) {
			System.out.println("Not acceptable coordinates\n");
			return false;
		}
		return true;
	}
	
	//takes in coordinates as string, returns ArrayList index as int
	public static int coordtoInt(String coord) {
		int file = coord.toLowerCase().charAt(0) - 97;
		int rank = Character.getNumericValue(coord.charAt(1));
		
		return ((rank - 1) * 8) + file;
	}
	
	//initializes ArrayList board with starting setup of pieces
	public static void setUp() {
		board.add(new Rook('w', 0));
		board.add(new Knight('w', 1));
		board.add(new Bishop('w', 2));
		board.add(new Queen('w', 3));
		board.add(new King('w', 4));
		wKing = 4;
		board.add(new Bishop('w', 5));
		board.add(new Knight('w', 6));
		board.add(new Rook('w', 7));
		for (int x = 8; x < 16; x++) {
			board.add(new Pawn('w', x));
		}
		
		for (int x = 16; x < 48; x++) {
			board.add(x, null);
		}
		
		for (int x = 48; x < 56; x++) {
			board.add(x, new Pawn('b', x));
		}
		board.add(56, new Rook('b', 56));
		board.add(57, new Knight('b', 57));
		board.add(58, new Bishop('b', 58));
		board.add(59, new Queen('b', 59));
		board.add(60, new King('b', 60));
		bKing = 60;
		board.add(61, new Bishop('b', 61));
		board.add(62, new Knight('b', 62));
		board.add(63, new Rook('b', 63));
	}
	
	//draws board using state of ArrayList board
	public static void drawBoard() {
		for (int x = 56; x >= 0; x -= 8) {
			for (int y = 0; y < 8; y++) {
				int z = x + y;
				
				if (board.get(z) == null) {
					if ((x / 8) % 2 == 0) {
						if (y % 2 == 0) {
							System.out.print("##");
						}
						else {
							System.out.print("  ");
						}
					}
					else {
						if (y % 2 == 0) {
							System.out.print("  ");
						}
						else {
							System.out.print("##");
						}
					}
				}
				else {
					if (board.get(z).color == 'w') {
						System.out.print("w");
					}
					else {
						System.out.print("b");
					}
					
					if (board.get(z) instanceof Rook) {
						System.out.print("R");
					}
					else if (board.get(z) instanceof Knight) {
						System.out.print("N");
					}
					else if (board.get(z) instanceof Bishop) {
						System.out.print("B");
					}
					else if(board.get(z) instanceof Queen) {
						System.out.print("Q");
					}
					else if(board.get(z) instanceof King) {
						System.out.print("K");
					}
					else if(board.get(z) instanceof Pawn) {
						System.out.print("P");
					}
				}
				System.out.print(" ");
			}
			System.out.println(" " + ((x / 8) + 1));
		}
		for(int z = 97; z < 105; z++) {
			System.out.print((char)z + "  ");
		}
		System.out.println("\n");
	}
}
