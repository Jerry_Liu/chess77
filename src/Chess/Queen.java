package Chess;

public class Queen extends Piece{

	public Queen(char color, int location) {
		super.color = color;
		super.location = location;
	}
	
	@Override
	public int[] move(int destination) {
		int [] checkSpaces;
		int [] legal = {};
		int [] illegal = {-1};
		
		if ((location % 8) == (destination % 8) || (location/8) == (destination/8)){
			int i = 0;
			int j = 0;
			int x;
			if ((location % 8) == (destination % 8)){ // vertical movement
				x = 8;
				i = (destination - location)/8;
				i = Math.abs(i);
				i = i-1;
				checkSpaces = new int[i];
				if(j == i){
					return legal;
				}
				while (j != i){
					if (location < destination){
						checkSpaces[j] = location + x;
						x = x + 8;
					}else{
						checkSpaces[j] = location - x;
						x = x + 8;
					}
					j++;
				}
				return checkSpaces;
		
			}else if ((location/8) == (destination/8)){ // horizontal movement
				
				x = 1;
				i = destination - location;
				i = Math.abs(i);
				i = i-1;
				checkSpaces = new int[i];
				if(j == i){
					return legal;
				}
				while (j != i){
					if (location < destination){
						checkSpaces[j] = location + x;
						x = x + 1;
					}else{
						checkSpaces[j] = location - x;
						x = x + 1;
					}
					j++;
				}
				return checkSpaces;
			}else{
				return illegal;
			}
		}else if((Math.abs(location - destination) % 7) == 0 || (Math.abs(location - destination) % 9 == 0)){
			int i = 0;
			int j = 0;
			int x = location;
			int y = location;
			int a = 0;
			if (location < destination){ 
				if((destination - location) % 7 == 0 ){
					j = 7;
				}else if((destination - location) % 9 == 0){
					j = 9;
				}else{
					return illegal;
				}
				do{
					x = x + j;
					i++;
				}while((x % 8) != 0 &&(x % 8) != 7 && x != destination);
				if( x != destination){
					return illegal;
				}
				if (i == 1){
					return legal;
				}
				checkSpaces = new int[i-1];
				
				while (a != (i-1)){
					checkSpaces[a] = y + j;
					y = y + j;
					a++;
				}
				return checkSpaces;
				
			}else{
				if((location - destination) % 7 == 0 ){
					j = 7;
				}else if((destination - location) % 9 == 0){
					j = 9;
				}else{
					return illegal;
				}
				do{
					x = x - j;
					i++;
				}while(x % 8 != 0 &&x % 8 != 7 && x != destination);
				if( x != destination){
					return illegal;
				}
				if (i == 1){
					return legal;
				}
				checkSpaces = new int[i-1];
				while (a != (i-1)){
					checkSpaces[a] = y - j;
					y = y-j;
					a++;
				}
				return checkSpaces;
				
			}
		}else{
			return illegal;
		}

	}

}
