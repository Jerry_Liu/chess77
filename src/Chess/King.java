package Chess;

public class King extends Piece{

	public King(char color, int location) {
		super.color = color;
		super.location = location;
	}
	
	@Override
	public int[] move(int destination) {
		int [] checkSpaces;
		checkSpaces = new int[8];
		checkSpaces[0] = location - 9;
		checkSpaces[1] = location - 8;
		checkSpaces[2] = location - 7;
		checkSpaces[3] = location - 1;
		checkSpaces[4] = location + 1;
		checkSpaces[5] = location + 7;
		checkSpaces[6] = location + 8;
		checkSpaces[7] = location + 9;
		
		int [] legal = new int[0];
		int [] illegal = {-1};

		if(location%8 == 0){
			if(destination == checkSpaces[1] || destination == checkSpaces[2] 
					|| destination == checkSpaces[4] || destination == checkSpaces[6] || destination == checkSpaces[7]){
				return legal;
			}else{
				return illegal;
			}
		}else if(location%8 == 7){
			if(destination == checkSpaces[0] || destination == checkSpaces[1] 
					|| destination == checkSpaces[3] || destination == checkSpaces[5] || destination == checkSpaces[6]){
				return legal;
			}else{
				return illegal;
			}
		}else{
			int i = 0;
			while (i != 8){
				if (destination == checkSpaces[i]){
					return legal;
				}
				i++;
			}
			
			// check castling
			if (moved == false && (Math.abs(destination-location)) == 2){
				//System.out.println(moved);
				if(destination > location){
					checkSpaces = new int[1];
					checkSpaces[0] = location +1;
					return checkSpaces;
				}else{
					checkSpaces = new int[2];
					checkSpaces[0] = location - 1;
					checkSpaces[1] = location - 3;
					return checkSpaces;
				}
				
			}
			return illegal;
		}
	}
}
