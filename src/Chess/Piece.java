package Chess;

/**
 * Piece is the abstract base class for all chess pieces. 
 * It contains the basic information needed including:
 *  <ul>
 *  <li> The piece color
 *  <li> The current location
 *  <li> The status stating if the piece has moved from it's original position
 *  </ul>
 *  <p>
 *  
 * @author Cynthia
 * @author Jerry
 *
 */

public abstract class Piece {
	protected char color;
	protected int location;
	protected boolean moved = false;
	
	/**
	 * Returns an array of spaces the piece much pass through to reach 
	 * its destination from its current location. This method also checks whether
	 * or not the target destination is a legal destination for the piece from its
	 * current location. If no spaces are between the target destination and its 
	 * current location and the move is legal, an empty array will be returned 
	 * declaring the move legal as long as the destination space is empty. 
	 * Otherwise, if the moved is deemed illegal, an array containing -1 will be 
	 * returned.
	 * 
	 * 
	 * @param destination 	An integer specifying the destination location of the piece
	 * @return				An integer array of spaces, an empty array or an array with -1
	 */
	
	public abstract int[] move(int destination);
	
	/**
	 * Accesses the piece's color and returns.
	 * 
	 * @return				The piece's color
	 */
	
	public char getColor() {
		return color;
	}
	
	/**
	 * Sets the piece's current location after a move has been executed.
	 * 
	 * @param location		An integer specifying the pieces new location on the board
	 */
	public void setLocation(int location) {
		this.location = location;
	}
	
	/**
	 * Accesses the piece's current location and returns.
	 * 
	 * @return				An integer for the pieces current location
	 */
	public int getLocation() {
		return location;
	}
	
	/**
	 * Changes the piece's state to indicate that it has been moved.
	 */
	public void setMoved() {
		moved = true;
	}
	
	/**
	 * Acesses the piece's current moved state and returns.
	 * 
	 * @return			Boolean stating if the piece has been moved.
	 */
	public boolean getMoved() {
		return moved;
	}
}
