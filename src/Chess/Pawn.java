package Chess;

public class Pawn extends Piece{
	
	public Pawn (char color, int location) {
		super.color = color;
		super.location = location;
	}

	@Override
	public int[] move(int destination) {
		int [] checkSpaces;
		int [] legal = {};
		int [] illegal = {-1};
		
		if(moved == false && Math.abs(location - destination) > 9){
			if (Math.abs(location - destination) != 16){
				return illegal;
			}else{
				checkSpaces = new int[1];
				if(color == 'w'){
					checkSpaces[0] = location + 8;
					return checkSpaces;	
				}else{
					checkSpaces[0] = location - 8;
					return checkSpaces;
				}
			}
		}
		if((Math.abs(destination -location) > 16) || (Math.abs(destination -location) < 7)){
			return illegal;
		}else{
			if(color == 'w'){
				if(location + 7 == destination || location + 8 == destination || location + 9 == destination){
					if (location % 8 == 0 && location + 7 == destination){
						return illegal;
					}
					if (location % 8 == 7 && location + 9 == destination){
						return illegal;
					}
					return legal;
				}
				return illegal;
			}else{
				if(location - 7 == destination || location - 8 == destination || location - 9 == destination){
					if (location % 8 == 0 && location - 9 == destination){
						return illegal;
					}
					if (location % 8 == 7 && location - 7 == destination){
						return illegal;
					}
					return legal;
				}
				return illegal;
			}
		}

		//return the starting space?
		//Need case for enpassant.
	}
}
