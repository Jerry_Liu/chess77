package Chess;

public class Rook extends Piece{
	
	public Rook(char color, int location) {
		super.color = color;
		super.location = location;
	}

	@Override
	public int[] move(int destination) {
		int [] checkSpaces;
		int [] legal = {};
		int [] illegal = {-1};
		
		int i = 0;
		int j = 0;
		int x;
		if ((location % 8) == (destination % 8)){ // vertical movement
			x = 8;
			i = (destination - location)/8;
			i = Math.abs(i);
			i = i-1;
			checkSpaces = new int[i];
			if(j == i){
				return legal;
			}
			while (j != i){
				if (location < destination){
					checkSpaces[j] = location + x;
					x = x + 8;
				}else{
					checkSpaces[j] = location - x;
					x = x + 8;
				}
				j++;
			}
			return checkSpaces;
	
		}else if ((location/8) == (destination/8)){ // horizontal movement
			
			x = 1;
			i = destination - location;
			i = Math.abs(i);
			i = i-1;
			checkSpaces = new int[i];
			if(j == i){
				return legal;
			}
			while (j != i){
				if (location < destination){
					checkSpaces[j] = location + x;
					x = x + 1;
				}else{
					checkSpaces[j] = location - x;
					x = x + 1;
				}
				j++;
			}
			return checkSpaces;
		}else{
			return illegal;
		}

	
	}

}
