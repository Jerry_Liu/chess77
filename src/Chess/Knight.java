package Chess;

public class Knight extends Piece{
	
	public Knight(char color, int location) {
		super.color = color;
		super.location = location;
	}

	@Override
	public int[] move(int destination) {
		int [] checkSpaces;
		checkSpaces = new int[8];
		checkSpaces[0] = location - 10;
		checkSpaces[1] = location - 17;
		checkSpaces[2] = location - 15;
		checkSpaces[3] = location - 6;
		checkSpaces[4] = location + 6;
		checkSpaces[5] = location + 15;
		checkSpaces[6] = location + 17;
		checkSpaces[7] = location + 10;
		
		int [] legal = {};
		int [] illegal = {-1};
		
		if (location%8 == 0){
			if (destination == checkSpaces[2] || destination == checkSpaces[3]
					|| destination == checkSpaces[6] || destination == checkSpaces[7]){
				return legal;
			}else{
				return illegal;
			}
		}else if(location%8 == 1){
			if (destination == checkSpaces[1] || destination == checkSpaces[2]
					|| destination == checkSpaces[3] || destination == checkSpaces[5]
							|| destination == checkSpaces[6] || destination == checkSpaces[7]){
				return legal;
			}else{
				return illegal;
			}
		}else if(location%8 == 6){
			if (destination == checkSpaces[0] || destination == checkSpaces[1]
					|| destination == checkSpaces[2] || destination == checkSpaces[4]
							|| destination == checkSpaces[5] || destination == checkSpaces[6]){
				return legal;
			}else{
				return illegal;
			}
		}else if(location%8 == 1){
			if (destination == checkSpaces[0] || destination == checkSpaces[1]
					|| destination == checkSpaces[4] || destination == checkSpaces[5]){
				return legal;
			}else{
				return illegal;
			}
		}else{
			int i = 0;
			while (i != 8){
				if (destination == checkSpaces[i]){
					return legal;
				}
				i++;
			}
			return illegal;
		}
		
		
	}

}
