package Chess;

/**
 * An object class for the Bishop piece. Implements the specific move pattern
 * specified for the bishop piece.
 * 
 * @author Cynthia Liu
 * @author Jerry Liu
 *
 */

public class Bishop extends Piece{
	
	/**
	 * Initializes the Bishop piece using parameters set in Piece class.
	 * 
	 * @param color			A char storing the piece color
	 * @param location		An int storing the piece's current location
	 */
	public Bishop(char color, int location) {
		super.color = color;
		super.location = location;
	}

	/**
	 * Implement the move pattern specific to the Bishop piece. 
	 * Only diagonals are legal moves.
	 * 
	 * @param location		An int storing the piece's current location
	 * @return				returns array
	 */
	@Override
	public int[] move(int destination) {
		int [] checkSpaces;
		int [] legal = {};
		int [] illegal = {-1};
		
		int i = 0;
		int j = 0;
		int x = location;
		int y = location;
		int a = 0;
		if (location < destination){ 
			if((destination - location) % 7 == 0 ){
				j = 7;
			}else if((destination - location) % 9 == 0){
				j = 9;
			}else{
				return illegal;
			}
			do{
				x = x + j;
				i++;
			}while((x % 8) != 0 &&(x % 8) != 7 && x != destination);
			if( x != destination){
				return illegal;
			}
			if (i == 1){
				return legal;
			}
			checkSpaces = new int[i-1];
			
			while (a != (i-1)){
				checkSpaces[a] = y + j;
				y = y + j;
				a++;
			}
			return checkSpaces;
			
		}else{
			if((location - destination) % 7 == 0 ){
				j = 7;
			}else if((destination - location) % 9 == 0){
				j = 9;
			}else{
				return illegal;
			}
			do{
				x = x - j;
				i++;
			}while(x % 8 != 0 &&x % 8 != 7 && x != destination);
			if( x != destination){
				return illegal;
			}
			if (i == 1){
				return legal;
			}
			checkSpaces = new int[i-1];
			while (a != (i-1)){
				checkSpaces[a] = y - j;
				y = y-j;
				a++;
			}
			return checkSpaces;
			
		}
	}

}
